package com.rss.Controller;
import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.github.sardine.Sardine;
import com.rss.dao.SardinConnection;
import com.rss.dao.rssdao;
import com.rss.exception.DocumentnotfoundException;
@RestController
public class RssCntroller {
	@Autowired rssdao dao;
	@Autowired
	 private Environment env;
	@Autowired
	SardinConnection con;
	//private static final String PATH =;
	@CrossOrigin
	//@PostMapping(path = "/reviewers")	
	@PostMapping(path = "/reviewers")
	@ResponseBody
	public Object webdav(String documentname)throws DocumentnotfoundException {		
		JSONObject obj=new JSONObject();
		JSONArray arr=new JSONArray();
		Map<String, Object>returnmap=new HashMap<String, Object>();
	    String jsonStr=null;
	    String serialized=null;
	   
		//AUTHENTICATION
	    Sardine sardine=con.ConnectToSardin();        
        String documentToFind =documentname;      
        //String documentToFind= "2019-02-13 annual information form 2018_webproof 002860029_review.pdf__db744d9f91d3c94faaabb3c69979ac7a";
        //%%%%%%
        //String documentToFind="2019-04-17 AIF 2019_webproof 0028130029 Team and Third Party Review and Translation 2019-04-25_SAreview.pdf__dd532a430dbb994d976ab04da38fe481/";
      //  String documentToFind="2019-02-13 manulife fund facts_asset allocation portfolio_webproof 002890029_review.pdf__aa146514dde067438f414be6d9a24d27/";
      //String documentToFind="2019-02-20 manulife fund facts_manulife private investment pool_webproof_review.pdf__0ee39f046e38421297deb8058291ece1/";
       //  String documentToFind="2019-02-13 annual information form 2018_webproof 002860029_review.pdf__db744d9f91d3c94faaabb3c69979ac7a/";
       // String documentToFind="PRO_Russell_RIC_Combined_review.pdf";
         //String documentToFind="2019-02-25 simplified prospectus 2018_webproof 002810029 002810029_review.pdf__f8be4a4c6f3945a899838d606469ee4e/";
         //String documentToFind="2019-02-25 simplified prospectus 2018_webproof 002810029 002810029_review.pdf__f8be4a4c6f3945a899838d606469ee4e/";
      
        //String documentToFind="2019-05-07 manulife fund facts_English_team review and translation_SAreview.pdf__b9742bfd71ef0349ae2f152aa5e4c3ee/";
         try {
			//check if the path returned exists on the server.
			String web=dao.readParentDir(env.getProperty("webdav.url"),documentToFind);
        	System.out.println("WebDAV path = "+web);
        	 
        	if (!web.isEmpty() && sardine.exists(web))
    		 {			
        		    //- converts the rss xmls to json.
    				obj=dao.readWebDavDir(dao.readParentDir(env.getProperty("webdav.url"),documentToFind));
    				System.out.println(obj.toString());
    				arr.put(obj);
    				returnmap.put("Response", obj);			
    		 }        		        			
			else
			{
				throw new DocumentnotfoundException("Document "+documentToFind+" not exist ");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}       
        try {
			sardine.shutdown();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        //System.out.println("obj tostring = "+obj.toString());
        
        
        return obj.toMap();
			}// end of reviewers
}
