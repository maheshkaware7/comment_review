package com.rss.dao;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import com.github.sardine.DavResource;
import com.github.sardine.Sardine;
import com.rss.exception.DocumentnotfoundException;

@Component
public class rssdao {
	@Autowired
	SardinConnection con;
	@Autowired
	private Environment env;
	JSONObject reviewer = null;
	String U_email = "";
	DavResource U_url = null;
	URI root = null;

	public String readParentDir(String rootDirName, String documentToFind) throws DocumentnotfoundException {
		String webdavPath = "";
		Sardine sardine = con.ConnectToSardin();
		try {
			// GET LISTS
			List<DavResource> resources = sardine.list(rootDirName);
			for (DavResource res : resources) {

				if (res.isDirectory()) {
					boolean isMatch = res.toString().replaceAll("0020", " ").toLowerCase()
							.contains(documentToFind.toLowerCase());
					if (isMatch) {
						String doctoDirname = res.toString();
						// read Users
						listUsers("https://review.docubuilder.com" + doctoDirname);
						webdavPath = "https://review.docubuilder.com" + doctoDirname;
					}

				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return webdavPath;
	}// end

	public JSONObject readWebDavDir(String dirName) {
		boolean flag = false;
		Sardine sardine = con.ConnectToSardin();
		JSONObject reviewersjson = new JSONObject();
		try {
			// GET LISTS
			reviewer = new JSONObject();
			List<DavResource> resources = sardine.list(dirName);
			for (DavResource res : resources) {
				Map<String, String> resMap = res.getCustomProps();
				// get meta of the file if any.
				/*
				 * for (Map.Entry<String, String> entry : resMap.entrySet()) {
				 * //System.out.println("Key = " + entry.getKey() + ", Value = " +
				 * entry.getValue()); }
				 */
				flag = res.toString().endsWith(".xml");
				if (flag) // check if its a XML Directory.
				{
					// READ XML and CONVERT TO JSON
					String email = res.toString();
					String emailkey = email.split("/")[3].replaceAll(".at.", "@").replaceAll(".xml", "");
					InputStream is = sardine.get("https://review.docubuilder.com" + res.toString());
					String xmlString = IOUtils.toString(is, "utf-8");
					is.close();
					reviewersjson = convertToJSON(xmlString, emailkey);
				} else {
					continue;
					// return BlankReviewer;
				}
				// getCustomproperties(res);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (reviewersjson.isEmpty()) {
			JSONObject NoReviewer = new JSONObject();
			reviewersjson.put("Noreviewer", "No Reviewers");
		}
		return reviewersjson;
	}// end

	public void listUsers(String url) {
		Sardine sardine = con.ConnectToSardin();
		try {
			if (sardine.exists(url)) {
				List<DavResource> resources = sardine.list(url);
				for (DavResource res : resources) {
					if (!res.isDirectory() && res.toString().endsWith(".xml")) {
						String uemail = res.toString().split("/")[3].replaceAll(".at.", "@").replaceAll(".xml", "");
						U_email = uemail;
						U_url = res;
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}// end listUsers
		// to get any custom properties set to the file.

	public void getCustomproperties(DavResource res) {
		Map<String, String> customProps = res.getCustomProps();
		// Use custom properties...
		String author = (String) customProps.get("author");
		String title = (String) customProps.get("title");
	}// end getCustomproperties

	public JSONObject convertToJSON(String xmlContent, String userEmail) {

		HashMap<String, String> DateMap = new HashMap<String, String>();
		System.out.println("UserEmail in convert start = " + userEmail);
		JSONObject emailjson = new JSONObject();
		String[] KeyArray = { "m:highlight", "m:text", "m:stamp", "m:link", "m:freetext", "m:caret", "m:ink",
				"m:underline","m:square","m:strikeout"};
		try {
			int PRETTY_PRINT_INDENT_FACTOR = 4;
			JSONObject xmlJSONObj = XML.toJSONObject(xmlContent);
			JSONObject rssjson = (JSONObject) xmlJSONObj.get("rss");
			JSONObject channeljson = (JSONObject) rssjson.get("channel");
			if (channeljson.has("readcomments")) {
				JSONObject readcomments_obj = new JSONObject();
				readcomments_obj = (JSONObject) channeljson.get("readcomments");
				System.out.println("readcomments_obj  = " + readcomments_obj.toString());
				System.out.println("readcommentsOnPage ccc = " + readcomments_obj.get("readcommentsOnPage"));
				
				Object checkobj=readcomments_obj.get("readcommentsOnPage");
				if(checkobj instanceof JSONArray){
					JSONArray readcommentsOnPage_array=(JSONArray) checkobj;
					System.out.println("arrar aaa = "+readcommentsOnPage_array.toString());
					
					for(int j=0;j<readcommentsOnPage_array.length();j++){
						JSONObject readcomment_obj=(JSONObject) readcommentsOnPage_array.get(j);
						Object arraycomment=readcomment_obj.get("Comment");
						if(arraycomment instanceof JSONArray){
							JSONArray comarr=(JSONArray) arraycomment;
							for (Object obj  :comarr) {
								JSONObject innerCommentDetail=(JSONObject) obj;
								DateMap.put(innerCommentDetail.get("content").toString(),innerCommentDetail.get("pubDate").toString());
								
							}
						}else{
							JSONObject innerCommentDetail=(JSONObject) arraycomment;
							DateMap.put(innerCommentDetail.get("content").toString(),innerCommentDetail.get("pubDate").toString());
						}
					}
				}else{
					JSONObject readcommentsOnPage = (JSONObject) readcomments_obj.get("readcommentsOnPage");
					Object singleObj = readcommentsOnPage.get("Comment");
					if(singleObj instanceof JSONArray) {
						JSONArray commentarray  =(JSONArray) singleObj;
						for (int m = 0; m < commentarray.length(); m++) {
							JSONObject pubDate = (JSONObject) commentarray.get(m);
							System.out.println("pubDate  = " + pubDate.get("pubDate"));
							System.out.println("pubDate content = " + pubDate.get("content"));
							DateMap.put(pubDate.get("content").toString(), pubDate.get("pubDate").toString());
						}
					}else {
						JSONObject innerCommentDetail=(JSONObject) singleObj;
						DateMap.put(innerCommentDetail.get("content").toString(),innerCommentDetail.get("pubDate").toString());
					}
					
					/*
					 * for (Map.Entry<String, String> entry : DateMap.entrySet())
					 * System.out.println("Key = " + entry.getKey() + ", Value = " +
					 * entry.getValue());
					 */
				}
			}else {
				System.out.println("null");
			}

			if (channeljson.has("item")) {
				Object itemobj = channeljson.get("item");
				JSONArray Rssfeedarray = new JSONArray();

				if (itemobj instanceof JSONArray) {
					Rssfeedarray = (JSONArray) itemobj;
					JSONArray rssdetailsarray = new JSONArray();
					for (int i = 0; i < Rssfeedarray.length(); i++) {
						JSONObject rssdetails = new JSONObject();
						JSONObject itemjson = (JSONObject) Rssfeedarray.get(i);
						// System.out.println("guid fro title json = "+titlejson.get("guid"));
						if(itemjson.has("guid")){
							String guid = itemjson.get("guid").toString();
							String[] splitarray = guid.split("-", 2);
							System.out.println("guid split = " + splitarray[0]);
							System.out.println("guid split = " + splitarray[1]);
							rssdetails.put("Page_no",splitarray[0]);
							if (DateMap.containsKey(splitarray[1])) {
								System.out.println("value for" + splitarray[1] + " =  " + DateMap.get(splitarray[1]));
								
								rssdetails.put("date",DateMap.get(splitarray[1]));
							}
						}
						
						rssdetails.put("Comment", itemjson.get("description"));
						
						
						/*
						 * if(!itemjson.isNull("pubDate")) {
						 * System.out.println("putdate = "+itemjson.get("pubDate"));
						 * rssdetails.put("creationdate",itemjson.get("pubDate")); } else {
						 * rssdetails.put("creationdate",""); }
						 */

						rssdetails.put("UserEmail", userEmail);
						for (int j = 0; j < KeyArray.length; j++) {
							if (itemjson.has(KeyArray[j])) {
								JSONObject keyjson = (JSONObject) itemjson.get(KeyArray[j]);

								rssdetails.put("type", keyjson.get("intent"));
							}

						}
						rssdetailsarray.put(rssdetails);
					}
					emailjson.put("rssdeatails", rssdetailsarray);
					reviewer.put(userEmail, emailjson);
				} else {
					JSONObject rssdetails = new JSONObject();
					JSONObject itemjson = (JSONObject) itemobj;
					if (!itemjson.isEmpty()) {
						JSONArray rssdetailsarray = new JSONArray();
						rssdetails.put("Comment", itemjson.get("description"));
						rssdetails.put("UserEmail", userEmail);
						String guid = itemjson.get("guid").toString();
						String[] splitarray = guid.split("-", 2);
						rssdetails.put("Page_no",splitarray[0]);
						if (DateMap.containsKey(splitarray[1])) {
							System.out.println("value for" + splitarray[1] + " =  " + DateMap.get(splitarray[1]));
							
							rssdetails.put("date",DateMap.get(splitarray[1]));
						}
						
						
						for (int j = 0; j < KeyArray.length; j++) {
							if (itemjson.has(KeyArray[j])) {
								JSONObject keyjson = (JSONObject) itemjson.get(KeyArray[j]);
								rssdetails.put("CreationDate", keyjson.get("creationdate"));
								rssdetails.put("type", keyjson.get("intent"));
							}
						}
						rssdetailsarray.put(rssdetails);
						emailjson.put("rssdeatails", rssdetailsarray);
						reviewer.put(userEmail, emailjson);
					} // check not empty
				} // end else is object
			} else {
				JSONObject rssdetails = new JSONObject();
				JSONArray rssdetailsarray = new JSONArray();
				emailjson.put("rssdeatails", rssdetailsarray);
				reviewer.put(userEmail, emailjson);
			}
			String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reviewer;
	}// end convertToJSON

}
